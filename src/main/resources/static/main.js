let data = []
const state = {
  selectedZoo: null,
  selectedEnclosure: null,
  newEnclosure: null,
  newSpecies: null
}

fetch(`/zoos`)
  .then(response => response.json())
  .then(_data => {
    data = _data
    render()
  })
  .catch(err => console.error(err))

let slideIndex = 1;

// Next and previous control
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {

  let i;
  let slides = document.getElementsByClassName("mySlides");
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  slideIndex++;
  if (slideIndex > slides.length) { slideIndex = 1 }
  slides[slideIndex - 1].style.display = "block";
  setTimeout(showSlides, 6000); // Change image every 6 seconds ish 
}



function render() {
  let content = data.map((zooData, i) => {
    return `
        <article class ="zoo-card">
            <div style="background-image: url('${zooData.imageURL}');"></div>
            <footer>
            
            <h2 id="${i}-header">${zooData.name}</h2>
            <h2 id="${i}-header">${zooData.location}</h2>
            <button class="modal-button"  onclick="displayUpdateZoo(${i})">Update</button>
            <button class="modal-button"  onclick="displayDeleteZoo(${i})">Remove</button>
            <button class="modal-button" onclick="displayEnclosures(${i})">Enclosures</button>
            <footer>
        </article>
        `
  }).join("")

  content += getAddFormHTML()


  const appEL = document.getElementById(`web`)
  appEL.innerHTML = content

  if (state.selectedZoo) {
    const modalContent = `
           <section class="modal-bg">
           <article>
           <main>
               ${state.selectedZoo.enclosures.map(enclosures => {
      return `
                   <article>
                   <h3>${enclosures.enclosureName}</h3>
                   <img class = "enclosurePhoto" src =${enclosures.imageURL}>
                   <button class="modal-button" onclick="loadUpdateEnclosuresForm(${state.selectedZoo.id})">Update</button>
                   <button class="modal-button" onsubmit="event.preventDefault();deleteEnclosure(this);"(${state.selectedZoo.id})">Delete</button>
                   <button class="accordion" onclick="openAccordian()">Species</button>
                   <div class="panel">   
                   ${enclosures.species.map(species => {
        return ` <dl class="accordian-species">
                <dd>- Image </dd>
                <dt><img class="enclosurePhoto" src=${species.imageURL}></dt>
                <dd>- Name</dd>
                <dt>${species.animalName}</dt>  
                <dd>- Species </dd>
                <dt>${species.species}</dt>
                <dd>- Status</dd>
                <dt>${species.status}</dt> 
                <dd>- Weight KG</dd>
                <dt>${species.weight}</dt> 
                </dl>
                
                
                       `
      })}
                   </div> 
                   <button class="modal-button" onclick="loadCreateSpeciesForm(${state.selectedZoo.id},${enclosures.id})">Create Species</button>
                   </article>
                   `
    }).join("")}
                  </main>
                  <footer> 
               <button class="modal-button" onclick="loadCreateEnclosuresForm(${state.selectedZoo.id})">Create enclosures</button>
                
               <button class="modal-button" onclick="closeModal()">close</button>

               </footer>
               </article>
           </section>
        `
        
    const modalEL = document.getElementById(`modal`)
    modalEL.innerHTML = modalContent
  } else {
    const modalEL = document.getElementById(`modal`)
    modalEL.innerHTML = ""
  }
  
}


function openAccordian() {
 
  var acc = document.getElementsByClassName("accordion");
 var i;
 for (i = 0; i < acc.length; i++) {
   acc[i].addEventListener("click", function () {
     this.classList.toggle("active");
     var panel = this.nextElementSibling;
     if (panel.style.maxHeight) {
       panel.style.maxHeight = null;
     } else {
       panel.style.maxHeight = panel.scrollHeight + "px";
     }
   });
 }
}



function closeModal() {
  state.selectedZoo = null
  state.modalContent = null
   document.getElementById('updateModalContainer').classList.remove('show')
   document.getElementById('deleteModalContainer').classList.remove('show')
  
  render()
}

//zoo stufff

function addZoo(HTMLform) {
  const form = new FormData(HTMLform)
  const name = form.get(`name`)
  const imageURL = form.get(`imageURL`)
  const location = form.get(`location`)
  fetch(`/zoos`, {
    method: `POST`,
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({ name, imageURL, location })
  })
    .then(res => res.json())
    .then(zoo => {
      data.push(zoo)
      render()
    })
    .catch(console.error)
}

function displayZoo(index) {
  state.zooOpen = data[index].zoo
  render()
}


function getAddFormHTML() {
  return `
      <article>
      <form onsubmit="event.preventDefault();addZoo(this);">
      <label>Zoo Name</label>
            <input class="inputBox" name= "name" required/>
            <label>ImageURL</label>
            <input class="inputBox" name= "imageURL" type="url" required />
            <label>location</label>
            <input class="inputBox" name= "location" required />
            <button class="modal-button" style="width : 13rem;">Add Zoo</button>
      </form>
      </article>
      `
}

function updateZoo(HTMLform) {
  const form = new FormData(HTMLform)
  const id = form.get(`id`)
  const name = form.get(`name`)
  const imageURL = form.get(`imageURL`)
  const location = form.get('location')
  fetch(`/zoos/${id}`, {
    method: `PUT`,
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({ name, imageURL, location })
  })
    .then(res => res.json())
    .then(zoo => {
      const index = data.findIndex(zoo => {
        return zoo.id.toString() === id.toString()
      })
      data[index] = zoo
      closeModal();
      render()
    })
    .catch(console.error)
}


function deleteZoo() {
    fetch(`/zoos/${state.selectedZoo.id}`, {
    method: `DELETE`,
    headers: {
      "Content-Type": "application/json"
    }
  })
    .then(() => {
      const index = data.findIndex(zoo => {
        return zoo.id.toString() === state.selectedZoo.id.toString()
      })
      data.splice(index, 1)
      closeModal();
      render()
    })
    .catch(console.error)
}

function displayUpdateZoo(index) {
  state.selectedZoo = data[index]
  state.modalContent = "update"
  document.getElementById('updateModalContainer').classList.add('show')
  document.querySelector('#updateModalContainer input[name="id"]').value=state.selectedZoo.id;
  document.querySelector('#updateModalContainer input[name="name"]').value=state.selectedZoo.name;
  document.querySelector('#updateModalContainer input[name="imageURL"]').value=state.selectedZoo.imageURL;
  document.querySelector('#updateModalContainer input[name="location"]').value=state.selectedZoo.location;
}

function displayDeleteZoo(index) {
  state.selectedZoo = data[index]
  document.getElementById('deleteModalContainer').classList.add('show')
 

  let content =  `
        <article class ="zoo-card">
            <div style="background-image: url('${state.selectedZoo.imageURL}');"></div>
            <footer>
            <h2 id="${index}-header"> Are you sure you wish to delete ${state.selectedZoo.name}</h2>
            <button class="modal-button" style="width :13rem;" onclick="deleteZoo()" >Remove Zoo</button>
            <button type="button" class="modal-button" id="closeDeleteModal" onclick="closeModal()" formnovalidate>Close</button>
            </footer>
        </article>
        `

 
  const appEL = document.getElementById(`deleteModalContainer`)
  appEL.innerHTML = content
}

//end of zoo

//enclosures  

function submitNewEnclosures(HTMLform) {
const form = new FormData(HTMLform)
const zoo_id = form.get("zoo_id")
const enclosureName = form.get("enclosureName")
const imageURL = form.get("imageURL")

  fetch(`/zoos/${zoo_id}/enclosures`, {
    method: 'POST',
    headers: {
      "Content-type": "application/json"
    },
    body: JSON.stringify({ enclosureName, imageURL })
  })
    .then(res => res.json())
    .then(enclosures => {
      enclosures, state.newEnclosure
      render()
    })
}

function displayEnclosures(index) {
  state.selectedZoo = data[index]
  render()
}

function loadCreateEnclosuresForm(zoo_id) {
  const createForm = ` 
  <form onsubmit="event.preventDefault(); submitNewEnclosures(this)"> 
  <h3>Create a new Enclosures</h3> 
  <input type="hidden" name="zoo_id" value="${zoo_id}" /> 
  <div> 
      <label>Enclosures name</label> 
      <input name="enclosureName"  required />
      <label>imageURL</label> 
      <input name="imageURL" required />
   </div> 
      <ul id="Enclosures-Species"></ul> 
   <button class="modal-button">Create</button> 
   </form> `
  const mainSectionInModal = document.querySelector('.modal-bg > article > main')
  mainSectionInModal.innerHTML = createForm
  console.log(zoo_id)
}

function displayEnclosures(index) {
  state.selectedZoo = data[index]
  render()
}


function displayUpdateEnclosures(index) {
  state.selectedZoo = data[index]
  state.modalContent = "update"
  document.getElementById('updateModalContainer').classList.add('show')
  document.querySelector('#updateModalContainer input[name="id"]').value=state.selectedZoo.id;
  document.querySelector('#updateModalContainer input[name="name"]').value=state.selectedZoo.name;
  document.querySelector('#updateModalContainer input[name="imageURL"]').value=state.selectedZoo.imageURL;
  document.querySelector('#updateModalContainer input[name="location"]').value=state.selectedZoo.location;
}

//function displayDeleteEnclosure(index) {
//  state.selectedZoo = data[index]
//  document.getElementById('deleteModalContainer').classList.add('show')
//  document.querySelector('#deleteModalContainer input[name="id"]').value=state.selectedZoo.id;
//}


//function loadUpdateEnclosuresForm(zoo_id) {
//  const createForm = ` 
//  <form onsubmit="event.preventDefault(); UpdateNewEnclosures(this)"> 
//  <h3>Update Enclosure</h3> 
//  <input type="hidden" name="zoo_id" value="${zoo_id}" /> 
//  <div> 
//      <label>Enclosures name</label> 
//      <input name="enclosureName"  required />
//      <label>imageURL</label> 
//      <input name="imageURL" required />
//   </div> 
//      <ul id="Enclosures-Species"></ul> 
//   <button class="modal-button" >Update</button> 
//   </form> `
//  const mainSectionInModal = document.querySelector('.modal-bg > article > main')
//  mainSectionInModal.innerHTML = createForm
//  console.log(zoo_id)
//}


//function UpdateNewEnclosures(HTMLform) {
//  const form = new FormData(HTMLform)
//  const zoo_id = form.get(`zoo_id`)
//  const id = form.get(`id`)
//  const name = form.get(`name`)
//  const imageURL = form.get(`imageURL`)
//  const location = form.get('location')
//  fetch(`/zoos/${zoo_id}/enclosures/${id}`, {
//    method: `PUT`,
//    headers: {
//      "Content-Type": "application/json"
//    },
//    body: JSON.stringify({ name, imageURL, location })
//  })
//    .then(res => res.json())
//    .then(enclosures => {
//      const index = data.findIndex(enclosures => {
//        return enclosures.id.toString() === id.toString()
//      })
//      data[index] = enclosures
//      closeModal();
//      render()
//    })
//    .catch(console.error)
//  }
//  
//
//function deleteEnclosure(HTMLform) {
//  const form = new FormData(HTMLform)
//  const id = form.get(`id`)
//  fetch(`/zoos/${zoo_id}/enclosures/${id}`, {
//    method: `DELETE`,
//    headers: {
//      "Content-Type": "application/json"
//    }
//  })
//      .then(() => {
//      const index = data.findIndex(enclosures => {
//        return enclosures.id.toString() === id.toString()
//      })
//      data.splice(index, 1)
//      closeModal()
//
//      
//
//      render()
//    })
//    .catch(console.error)
//}

//end of enclosures

//species  

function submitNewSpecies(HTMLform) {
  const form = new FormData(HTMLform)
  const zoo_id = form.get("zoo_id")
  const enclosures_id = form.get("enclosures_id")
  const imageURL = form.get("imageURL")
  const animalName = form.get("animalName")
  const species = form.get("species")
  const status = form.get("status")
  const weight = form.get("weight")
  
    fetch(`/zoos/${zoo_id}/enclosures/${enclosures_id}/species`, {
      method: 'POST',
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify({imageURL,animalName,species,status,weight })
    })
      .then(res => res.json())
      .then(species => {
        console.log(species)
       const index = state.selectedZoo.enclosures.findIndex(enclosures => enclosures.id === enclosures_id)
        state.selectedZoo.enclosures[index].species.push(species)
        render()
      })
  }
  
  
  function loadCreateSpeciesForm(zoo_id,enclosures_id) {
    const createForm = ` 
    <form onsubmit="event.preventDefault(); submitNewSpecies(this)"> 
    <h3>Create a new Species</h3> 
    <input type="hidden" name="zoo_id" value="${zoo_id}" /> 
    <input type="hidden" name="enclosures_id" value="${enclosures_id}" /> 
    <div> 
        <label>imageUrl</label> 
        <input name="imageURL" required />
        <label>Animal Name</label> 
        <input name="animalName" required />
        <label>species</label> 
        <input name="species" required />
        <label>Endangered status</label> 
        <input name="status" required />
        <label>weight</label> 
        <input name="weight" required />
     </div> 
        <ul id="species"></ul> 
     <button class="modal-button">Create</button> 
     </form> `
    const mainSectionInModal = document.querySelector('.modal-bg > article > main')
    mainSectionInModal.innerHTML = createForm
    console.log(zoo_id)
  }

  //end of species

  

  
  showSlides(slideIndex);