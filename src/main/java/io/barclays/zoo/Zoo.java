package io.barclays.zoo;

import java.util.List;
import javax.persistence.*;

@Entity
public class Zoo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private String imageURL;
    private String location;
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "zoo_id")

    private List<Enclosures> enclosures;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public List<Enclosures> getEnclosures() {
        return enclosures;
    }

}