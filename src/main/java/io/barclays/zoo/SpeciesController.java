package io.barclays.zoo;

import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class SpeciesController {
    private SpeciesRepository repo;
    private EnclosuresRepository enclosuresRepo;

    public SpeciesController(SpeciesRepository repo, EnclosuresRepository enclosuresRepo) {
        this.repo = repo;
        this.enclosuresRepo = enclosuresRepo;
    }

    @GetMapping("/zoos/{zoo_id}/enclosures/{enclosures_id}/species")
    public List<Species> getSpecies() {
        return this.repo.findAll();
    }

    @PostMapping("/zoos/{zoo_id}/enclosures/{enclosures_id}/species")
    public Species addSpecies(@RequestBody Species SpeciesData, @PathVariable Integer enclosures_id) {
        Enclosures enclosures = enclosuresRepo.findById(enclosures_id).get();
        SpeciesData.setEnclosures(enclosures);
        return repo.save(SpeciesData);
    }

    @DeleteMapping("/zoos/{zoo_id}/enclosures/{enclosures_id}/species/{id}")
    public void deleteOne(@PathVariable Integer zoo_id, @PathVariable Integer enclosures_id, @PathVariable Integer id) {
        repo.deleteById(id);
    }

    @PutMapping("/zoos/{zoo_id}/enclosures/{enclosures_id}/species/{id}")
    public Species updateOne(@RequestBody Species speciesUpdate, @PathVariable Integer zoo_id,
            @PathVariable Integer enclosures_id, @PathVariable Integer id) {
        Optional<Species> optionalSpecies = repo.findById(id);
        try {
            Species species = optionalSpecies.get();
            species.setAnimalName(speciesUpdate.getAnimalName());
            species.setImageURL((speciesUpdate.getImageURL()));
            species.setSpecies(speciesUpdate.getSpecies());
            species.setStatus(speciesUpdate.getStatus());
            species.setWeight(speciesUpdate.getWeight());
            return repo.save(species);

        } catch (Exception err) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "entity not found");
        }
    }

}