package io.barclays.zoo;

import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class EnclosuresController {

    private EnclosuresRepository repo;
    private ZooRepository zooRepo;

    public EnclosuresController(EnclosuresRepository repo, ZooRepository zooRepo) {
        this.repo = repo;
        this.zooRepo = zooRepo;
    }

    @GetMapping("/zoos/{zoo_id}/enclosures")
    public List<Enclosures> getEnclosuress() {
        return this.repo.findAll();
    }

    @PostMapping("/zoos/{zoo_id}/enclosures")
    public Enclosures addEnclosure(@RequestBody Enclosures enclosuresData, @PathVariable Integer zoo_id) {
        Zoo zoo = zooRepo.findById(zoo_id).get();
        enclosuresData.setZoo(zoo);
        return repo.save(enclosuresData);
    }

    @PutMapping("/zoos/{zoo_id}/enclosures/{id}")
    public Enclosures updateOne(@RequestBody Enclosures enclosuresUpdate, @PathVariable Integer zoo_id,
            @PathVariable Integer id) {
        Optional<Enclosures> optionalEnclosure = repo.findById(id);
        try {
            Enclosures enclosure = optionalEnclosure.get();
            enclosure.setEnclosureName(enclosuresUpdate.getEnclosureName());
            enclosure.setImageURL(enclosuresUpdate.getImageURL());

            return repo.save(enclosure);

        } catch (Exception err) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "entity not found");
        }
    }

    @DeleteMapping("/zoos/{zoo_id}/enclosures/{id}")
    public void deleteOne(@PathVariable Integer zoo_id, @PathVariable Integer id) {
        repo.deleteById(id);
    }

}
