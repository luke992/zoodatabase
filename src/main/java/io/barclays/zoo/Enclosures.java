package io.barclays.zoo;

import javax.persistence.Entity;

import java.util.List;

import javax.persistence.*;

@Entity
public class Enclosures {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String enclosureName;
    private String imageURL;
     @ManyToOne
     private Zoo zoo;

    @JoinColumn(name = "enclosures_id")
    @OneToMany(fetch = FetchType.EAGER)
    private List<Species> species;

    public List<Species> getSpecies() {
        return species;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEnclosureName() {
        return enclosureName;
    }

    public void setEnclosureName(String enclosureName) {
        this.enclosureName = enclosureName;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

     public void setZoo(Zoo zoo) {
     this.zoo = zoo;
     }

}
