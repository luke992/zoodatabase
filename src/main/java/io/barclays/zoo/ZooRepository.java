package io.barclays.zoo;

import org.springframework.data.jpa.repository.JpaRepository;

interface ZooRepository extends JpaRepository<Zoo, Integer> {
    
}
