package io.barclays.zoo;

import org.springframework.data.jpa.repository.JpaRepository;

interface EnclosuresRepository extends JpaRepository<Enclosures, Integer> {

}
