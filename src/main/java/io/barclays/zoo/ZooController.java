package io.barclays.zoo;

import java.util.List;

import org.springframework.web.bind.annotation.*;
   
@RestController
public class ZooController{
private ZooRepository repository;
    
        public ZooController(ZooRepository repository) {
            this.repository = repository;
        }
    
        @GetMapping("/zoos")
        public List<Zoo> getzoos() {
            return this.repository.findAll();
        }
    
        @PostMapping("/zoos")
        public Zoo createZoo(@RequestBody Zoo newZoo) {
            return repository.save(newZoo);
        }
    
        @GetMapping("/zoos/{id}")
        public Zoo getOne(@PathVariable Integer id) {
            return repository.findById(id).get();
        }
    
        @PutMapping("/zoos/{id}")
        public Zoo updateOne(@PathVariable Integer id, @RequestBody Zoo ZooUpdate) {
            return repository.findById(id).map(Zoo -> {
                Zoo.setName(ZooUpdate.getName());
                Zoo.setImageURL(ZooUpdate.getImageURL());
                Zoo.setLocation(ZooUpdate.getLocation());
                return repository.save(Zoo);
            }).orElseThrow();
        }
    
        @DeleteMapping("/zoos/{id}")
        public void deleteOne(@PathVariable Integer id) {
            repository.deleteById(id);
        }
    
    }



