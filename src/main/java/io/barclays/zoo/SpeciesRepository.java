package io.barclays.zoo;
import org.springframework.data.jpa.repository.JpaRepository;

interface SpeciesRepository extends JpaRepository<Species, Integer>  {
    
}

