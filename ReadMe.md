Steps for using bit bucket.

I would recommend using GitBash but if you have a preferred on thats fine.

Clone repo via clicking clone copy and paste into gitbash full instructions coming should automatically set up a file with same name of project and folder

go to where the folder is in your device then right click git bash here may have to click show more options for this to display.

make any change to the code in vs code

in git bash type git checkout -b lukestitle (this is just an example can type what you like the branch to be called )

when ready to commit type git add -A

then git commit -m "insert commit comments for example "firstcommit""

git push origin lukestitle

go to bitbucket and create pull request add reviewer if able if not we can just look over it

then merge and approve changes by clicking approve and merge via the ..

to get the latest changes from the master if someone has made changes git pull origin master in git bash that will give you the latest code


links to video and code from bootcamp that may help not all there.

https://www.loom.com/share/30d8b3e7e53f40b28f7f12a70f36b309 (post via interface to server)
https://bitbucket.org/bernardmordan/springboot-cafes/src/master/ (springboot server)
https://www.loom.com/share/680a5a70388d4ddf99e5e9aa115e28b9 (grid)
https://www.loom.com/share/1662c1889b304eeca26b60057ae77265 (adding modal interface
https://www.loom.com/share/41b8f6fc2dac4f0c9a4f4c3a6037b306 (fetching data in browser)
https://www.loom.com/share/9afbebe0baf541569cb7c46f17afeb9f ( using postman updating sqlitedatabase)
https://www.loom.com/share/1aa230c267e44ee28696c210faabaf80 (aws deploy sign in)
https://www.loom.com/share/537dbc808a724f7092cdbef91e23b4ba (creating a restaurant)
https://www.loom.com/share/c418957c23144f2f85f50055ea9d819d (return menus via postman)
https://www.loom.com/share/2baca2ba831c40599928290ffc1ee2c2(getting rest via postman)
https://www.loom.com/share/8ea4af2b4ef640a386548afd9ca9207a(project into jar file)
https://bitbucket.org/bernardmordan/springboot-restaurants-server/src/master/ (springboot server bit bucket)
https://bitbucket.org/bernardmordan/springboot-cafes/src/master/ (bitbucket week 3 deploying and displaying browser)
https://bitbucket.org/bernardmordan/db-connection/src/master/ (db connection bit bucket)
https://bitbucket.org/bernardmordan/java-restaurants/src/master/ (object relational mapping 
https://www.loom.com/share/70e37eaedaa445d385aa232d23d5456c (add menu to database maybe items doesnt end finished)